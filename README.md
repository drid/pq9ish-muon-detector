# Muon detector in PQ9ISH format

This project is an implementation of the Cosmic Watch muon detector adapted in the PQ9ISH form factor.

Arduino is replaced by STM32F0

Data is sent via CAN bus and/or UART

For details about Cosmic Watch: 

https://github.com/spenceraxani/CosmicWatch-Desktop-Muon-Detector-v2

## Attribution

MICROFC part was created by SnapEDA
